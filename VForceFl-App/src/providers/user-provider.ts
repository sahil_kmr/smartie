import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { User } from '../models/user';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class UserProvider {
    result: any;
    constructor(public http: Http) {
        console.log('Hello UserProvider Provider');
    }

    registerUser(user: User) {
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        let option = new RequestOptions({
            headers: headers
        });
        let body = JSON.stringify(user);
        return this.http.post('http://localhost:55434/api/Users', body, option).map(res => res.json());
    }
}
