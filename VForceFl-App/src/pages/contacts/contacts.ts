import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Contact, Contacts, ContactFieldType } from 'ionic-native';
import { Search } from "../../pipes/search";


/*
  Generated class for the Contacts page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-contacts',
    templateUrl: 'contacts.html'
})
export class ContactsPage {

    contactsFound: any;
    selectedContacts: Array<any>;
    term: string;

    constructor(public navCtrl: NavController,
        public navParams: NavParams) {
        this.selectedContacts = [];
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ContactsPage');
    }

    searchFn(ev: any) {
        this.term = ev.target.value;
    }

    selectContacntForImport(item: Contact, event: any) {
        let itemExist = false;
        let index = -1;
        this.selectedContacts.forEach((obj, idx) => {
            if (obj.id == item.id) {
                itemExist = true;
                index = idx;
            }
        });

        if (!itemExist && event._checked)
            this.selectedContacts.push(item);
        else if (itemExist && !event._checked)
            this.selectedContacts.splice(index, 1);

    }
    selectContacntForImport1(item) {
        this.selectedContacts = this.contactsFound.filter(x => x.isSelected == true);
    }

    selectAll() {
        this.contactsFound.forEach(item => {
            item.isSelected = true;
        })
        this.selectedContacts = this.contactsFound.filter(x => x.isSelected == true);
    }

    getJson(item) {
        alert(JSON.stringify(item));
    }

    getAllContact() {
        alert('i m called')
        //let fields: ContactFieldType[] = ['addresses', 'birthday', 'categories', 'country', 'department', 'displayName', 'emails', 'familyName', 'formatted', 'givenName', 'honorificPrefix', 'honorificSuffix',
        //    'id', 'ims', 'locality', 'middleName', 'name', 'nickname', 'note', 'organizations', 'phoneNumbers', 'photos', 'postalCode', 'region', 'streetAddress', 'title', 'urls'];

        let fields: ContactFieldType[] = ['displayName'];

        Contacts.find(fields).then((contacts) => {
            contacts.sort(function (a, b) {
                return a.displayName.toLowerCase() > b.displayName.toLowerCase() ? 1 : -1;
            });

            this.contactsFound = contacts;
            console.log(this.contactsFound);
        });
    }
}
