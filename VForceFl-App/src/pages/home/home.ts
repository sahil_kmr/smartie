import { Component } from '@angular/core';
import { User } from '../../models/User';
import { NavController, AlertController } from 'ionic-angular';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { UserProvider } from '../../providers/user-provider';
import { VerifyPhonePage } from '../verify-phone/verify-phone';
import 'rxjs/Rx';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    user: User;
    result: any;
    signUpForm: FormGroup;
    submitAttempt: boolean;
    constructor(public navCtrl: NavController,
        private http: Http,
        private userProvider: UserProvider,
        public formBuilder: FormBuilder,
        public alertController: AlertController) {

        this.user = {
            FullName: '',
            Email: '',
            Mobile: null
        };

        this.signUpForm = formBuilder.group({
            fullName: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            email: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), Validators.required])],
            mobile: ['']
        });
    }

    regiesterUser() {
        this.submitAttempt = true;
        console.log(this.signUpForm.valid)

        if (!this.signUpForm.valid)
            return;

        this.userProvider.registerUser(this.user).
            subscribe(
            data => {
                this.result = data;
                this.navCtrl.push(VerifyPhonePage, this.result);
            },
            error => {
                let confirm = this.alertController.create({
                    title: 'Message',
                    message: 'Phone Number Not Valid',
                    buttons: [{
                        text: 'Ok'
                    }]
                });
                confirm.present();
            });

    }

}