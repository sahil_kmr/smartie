import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DetailPage } from '../detail/detail';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/*
  Generated class for the VerifyPhone page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-verify-phone',
    templateUrl: 'verify-phone.html'
})
export class VerifyPhonePage {
    otp: any;
    mobile: any;
    otpNumber: any;
    otpForm: FormGroup;
    submitAttempt: boolean;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public alertController: AlertController,
        public formBuilder: FormBuilder) {
        this.otp = navParams.data.OTP;
        this.mobile = navParams.data.Mobile;
        this.otpNumber = '';

        this.otpForm = formBuilder.group({
            otp: ['', Validators.compose([Validators.maxLength(4), Validators.required])],
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad VerifyPhonePage');
    }

    verifyOTP() {
        if (this.otpNumber == this.otp) {
            this.navCtrl.push(DetailPage);
        }
        else {
            let confirm = this.alertController.create({
                title: 'Message',
                message: 'OTP not matched',
                buttons: [{ text: 'Ok' }]
            });
            confirm.present();
        }

    }

}
