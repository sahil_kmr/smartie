import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { VerifyPhonePage } from '../pages/verify-phone/verify-phone';
import { UserProvider } from '../providers/user-provider';
import { DetailPage } from '../pages/detail/detail';
import { ContactsPage } from '../pages/contacts/contacts';
import { Search } from '../pipes/search';



@NgModule({
    declarations: [
        MyApp,
        HomePage,
        VerifyPhonePage,
        DetailPage,
        ContactsPage,
        Search
    ],
    imports: [
        IonicModule.forRoot(MyApp)
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        VerifyPhonePage,
        DetailPage,
        ContactsPage
    ],
    providers: [UserProvider, { provide: ErrorHandler, useClass: IonicErrorHandler }]
})
export class AppModule { }
